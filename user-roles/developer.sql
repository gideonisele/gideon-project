\set testdb1_developer :'testdb1_developer'
\echo '** creating role ':testdb1_developer

CREATE ROLE testdb1_developer WITH 
    LOGIN 
    NOSUPERUSER 
    NOCREATEDB 
    NOCREATEROLE 
    INHERIT 
    NOREPLICATION;


