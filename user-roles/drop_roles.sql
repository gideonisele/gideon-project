\echo '** dropping user roles'

\set testdb1_advanced :'testdb1_advanced'
\echo '** deleting role ':testdb1_advanced
drop role testdb1_advanced;

\set tesdb1_dba :'tesdb1_dba'
\echo '** deleting role ':tesdb1_dba
drop role tesdb1_dba;

\set tesdb1_developer :'tesdb1_developer'
\echo '** deleting role ':tesdb1_developer
drop role tesdb1_developer;

