\set testdb1_dba :'testdb1_dba'
\echo '** creating role' :testdb1_dba

CREATE ROLE testdb1_dba WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION;
 
 