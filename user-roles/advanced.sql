\set testdb1_advanced :'testdb1_advanced'
\echo '** creating role' :testdb1_advanced

CREATE ROLE testdb1_advanced WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION;

  

  
