-- create owner role
CREATE ROLE dummy_app LOGIN PASSWORD 'changeme!';


-- create 'dummy' database
CREATE DATABASE dummy OWNER dummy_app;

-- default is no connection
REVOKE CONNECT ON DATABASE dummy FROM PUBLIC;
