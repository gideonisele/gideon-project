\echo "** running objects-001"

-- create sequences
CREATE SEQUENCE public.table1_user_id_seq;

ALTER SEQUENCE public.table1_user_id_seq
    OWNER TO postgres;

CREATE SEQUENCE public.table2_role_id_seq;

ALTER SEQUENCE public.table2_role_id_seq
    OWNER TO postgres;


CREATE TABLE public.table1
(
    user_id integer NOT NULL DEFAULT nextval('table1_user_id_seq'::regclass),
    username character varying(50) COLLATE pg_catalog."default" NOT NULL,
    password character varying(50) COLLATE pg_catalog."default" NOT NULL,
    email character varying(355) COLLATE pg_catalog."default" NOT NULL,
    created_on timestamp without time zone NOT NULL,
    last_login timestamp without time zone,
    CONSTRAINT table1_pkey PRIMARY KEY (user_id),
    CONSTRAINT table1_email_key UNIQUE (email),
    CONSTRAINT table1_username_key UNIQUE (username)
)

TABLESPACE pg_default;

ALTER TABLE public.table1
    OWNER to postgres;


CREATE TABLE public.table2
(
    role_id integer NOT NULL DEFAULT nextval('table2_role_id_seq'::regclass),
    role_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT table2_pkey PRIMARY KEY (role_id),
    CONSTRAINT table2_role_name_key UNIQUE (role_name)
)

TABLESPACE pg_default;

ALTER TABLE public.table2
    OWNER to postgres;



CREATE TABLE public.ref_table1
(
    user_id integer NOT NULL,
    role_id integer NOT NULL,
    grant_date timestamp without time zone,
    CONSTRAINT ref_table1_pkey PRIMARY KEY (user_id, role_id),
    CONSTRAINT account_role_role_id_fkey FOREIGN KEY (role_id)
        REFERENCES public.table2 (role_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT account_role_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public.table1 (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.ref_table1
    OWNER to postgres;
