CREATE ROLE dba_user WITH
	LOGIN
	NOSUPERUSER
	CREATEDB
	CREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'dba_user';
	
	GRANT testdb1_advanced_role, testdb1_dba_role, testdb1_developer_role TO dba_user;


