CREATE ROLE user2 WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  CREATEROLE
  NOREPLICATION
  ENCRYPTED PASSWORD 'user2';
  
  GRANT testdb1_advanced_role, testdb1_dba_role, testdb1_developer_role TO user2;

