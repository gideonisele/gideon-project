CREATE ROLE user3 WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  CREATEROLE
  NOREPLICATION
  ENCRYPTED PASSWORD 'user3';
  
 GRANT testdb1_advanced_role, testdb1_dba_role, testdb1_developer_role TO user3;

