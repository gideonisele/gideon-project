\echo '** creating database testdb2..'

-- Create database testdb1
CREATE DATABASE testdb2 WITH OWNER = postgres ENCODING = 'UTF8'  TABLESPACE = pg_default CONNECTION LIMIT = -1;

-- Revoke connection from all users
REVOKE CONNECT ON DATABASE testdb2 FROM public;