\echo '** creating database testdb1..'

-- Create database testdb1
CREATE DATABASE testdb1 WITH OWNER = postgres ENCODING = 'UTF8'  TABLESPACE = pg_default CONNECTION LIMIT = -1;

-- Revoke connection from all users
REVOKE CONNECT ON DATABASE testdb1 FROM public;
REVOKE CREATE ON SCHEMA public FROM PUBLIC;

\c testdb1

-- create sequences
CREATE SEQUENCE public.table1_user_id_seq;

ALTER SEQUENCE public.table1_user_id_seq
    OWNER TO postgres;

CREATE SEQUENCE public.table2_role_id_seq;

ALTER SEQUENCE public.table2_role_id_seq
    OWNER TO postgres;


CREATE TABLE public.table1
(
    user_id integer NOT NULL DEFAULT nextval('table1_user_id_seq'::regclass),
    username character varying(50) COLLATE pg_catalog."default" NOT NULL,
    password character varying(50) COLLATE pg_catalog."default" NOT NULL,
    email character varying(355) COLLATE pg_catalog."default" NOT NULL,
    created_on timestamp without time zone NOT NULL,
    last_login timestamp without time zone,
    CONSTRAINT table1_pkey PRIMARY KEY (user_id),
    CONSTRAINT table1_email_key UNIQUE (email),
    CONSTRAINT table1_username_key UNIQUE (username)
)

TABLESPACE pg_default;

ALTER TABLE public.table1
    OWNER to postgres;


CREATE TABLE public.table2
(
    role_id integer NOT NULL DEFAULT nextval('table2_role_id_seq'::regclass),
    role_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT table2_pkey PRIMARY KEY (role_id),
    CONSTRAINT table2_role_name_key UNIQUE (role_name)
)

TABLESPACE pg_default;

ALTER TABLE public.table2
    OWNER to postgres;



CREATE TABLE public.ref_table1
(
    user_id integer NOT NULL,
    role_id integer NOT NULL,
    grant_date timestamp without time zone,
    CONSTRAINT ref_table1_pkey PRIMARY KEY (user_id, role_id),
    CONSTRAINT account_role_role_id_fkey FOREIGN KEY (role_id)
        REFERENCES public.table2 (role_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT account_role_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public.table1 (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.ref_table1
    OWNER to postgres;

\echo '** running objects 002'

ALTER TABLE public.table1
ADD COLUMN last_name character varying(50);   


\set testdb1_app_connect :'testdb1_app_connect'
\echo '** creating role' :testdb1_app_connect

CREATE ROLE testdb1_app_connect WITH 
      NOLOGIN 
      NOSUPERUSER 
      INHERIT 
      NOCREATEDB 
      NOCREATEROLE 
      NOREPLICATION; 
      
-- this role will be used for creating tables and indexes
\set testdb1_app_create_objects :'testdb1_app_create_objects'
\echo '** creating role' :testdb1_app_create_objects

CREATE ROLE testdb1_app_create_objects WITH 
      NOLOGIN 
      NOSUPERUSER 
      INHERIT 
      NOCREATEDB 
      NOCREATEROLE 
      NOREPLICATION; 
      
\set testdb1_app_read_reference :'testdb1_app_read_reference'
\echo '** creating role' :testdb1_app_read_reference

CREATE ROLE testdb1_app_read_reference WITH 
      NOLOGIN 
      NOSUPERUSER 
      INHERIT 
      NOCREATEDB 
      NOCREATEROLE 
      NOREPLICATION;
      
      
 \set testdb1_app_read :'testdb1_app_read'
\echo '** creating role' :testdb1_app_read

CREATE ROLE testdb1_app_read WITH
  NOLOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION;
  
  
\set testdb1_app_write :'testdb1_app_write'
\echo '** creating role' :testdb1_app_write


CREATE ROLE testdb1_app_write
       WITH 
       NOLOGIN 
       NOSUPERUSER 
       INHERIT 
       NOCREATEDB 
       NOCREATEROLE 
       NOREPLICATION;
       
       


-- create user-roles
\set testdb1_advanced :'testdb1_advanced'
\echo '** creating role' :testdb1_advanced

CREATE ROLE testdb1_advanced WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION;
  
\set testdb1_dba :'testdb1_dba'
\echo '** creating role' :testdb1_dba

CREATE ROLE testdb1_dba WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION;
  
  
\set testdb1_developer :'testdb1_developer'
\echo '** creating role ':testdb1_developer

CREATE ROLE testdb1_developer WITH 
    LOGIN 
    NOSUPERUSER 
    NOCREATEDB 
    NOCREATEROLE 
    INHERIT 
    NOREPLICATION;
    


-- create users



-- app_connect grants
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_connect;


-- app_read grants
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_read;
GRANT USAGE ON SCHEMA public TO testdb1_app_read;
GRANT SELECT ON ALL TABLES in SCHEMA public TO testdb1_app_read;
GRANT EXECUTE ON ALL functions in SCHEMA public TO testdb1_app_read;
GRANT USAGE ON ALL sequences in SCHEMA public TO testdb1_app_read;


-- app_write grants
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_write;
GRANT USAGE ON SCHEMA public TO testdb1_app_write;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO testdb1_app_write;


-- app_read_reference grants
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_read_reference;
GRANT USAGE ON SCHEMA public TO testdb1_app_read_reference;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO testdb1_app_read_reference;
GRANT REFERENCES ON ALL TABLES IN SCHEMA public TO testdb1_app_read_reference;


-- app_create_objects grants
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_create_objects;
GRANT USAGE ON SCHEMA public TO testdb1_app_create_objects;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO testdb1_app_create_objects;
GRANT CREATE ON DATABASE testdb1 TO testdb1_app_create_objects;
