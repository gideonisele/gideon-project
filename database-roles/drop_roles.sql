\echo '** dropping database roles'

\set testdb1_app_read :'testdb1_app_read'
\echo '** deleting role' :testdb1_app_read
drop role testdb1_app_read;


\set testdb1_app_write :'testdb1_app_write'
\echo '** deleting role' :testdb1_app_write
drop role testdb1_app_write;

\set testdb1_app_read_reference :'testdb1_app_read_reference'
\echo '** deleting role' :testdb1_app_read_reference
drop role testdb1_app_read_reference;

\set testdb1_app_connect :'testdb1_app_connect'
\echo '** deleting role' :testdb1_app_connect
drop role testdb1_app_connect;

\set testdb1_app_create_objects :'testdb1_app_create_objects'
\echo '** deleting role' :testdb1_app_create_objects
drop role testdb1_app_create_objects;
