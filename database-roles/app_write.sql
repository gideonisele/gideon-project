\set testdb1_app_write :'testdb1_app_write'
\echo '** creating role' :testdb1_app_write


CREATE ROLE testdb1_app_write
       WITH 
       NOLOGIN 
       NOSUPERUSER 
       INHERIT 
       NOCREATEDB 
       NOCREATEROLE 
       NOREPLICATION;
       

  