\set testdb1_app_read :'testdb1_app_read'
\echo '** creating role' :testdb1_app_read

CREATE ROLE testdb1_app_read WITH
  NOLOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION;

  
