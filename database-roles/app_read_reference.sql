\set testdb1_app_read_reference :'testdb1_app_read_reference'
\echo '** creating role' :testdb1_app_read_reference

CREATE ROLE testdb1_app_read_reference WITH 
      NOLOGIN 
      NOSUPERUSER 
      INHERIT 
      NOCREATEDB 
      NOCREATEROLE 
      NOREPLICATION;
  