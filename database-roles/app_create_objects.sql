-- this role will be used for creating tables and indexes
\set testdb1_app_create_objects :'testdb1_app_create_objects'
\echo '** creating role' :testdb1_app_create_objects

CREATE ROLE testdb1_app_create_objects WITH 
      NOLOGIN 
      NOSUPERUSER 
      INHERIT 
      NOCREATEDB 
      NOCREATEROLE 
      NOREPLICATION; 
      

