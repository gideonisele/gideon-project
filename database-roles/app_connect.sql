\set testdb1_app_connect :'testdb1_app_connect'
\echo '** creating role' :testdb1_app_connect

CREATE ROLE testdb1_app_connect WITH 
      NOLOGIN 
      NOSUPERUSER 
      INHERIT 
      NOCREATEDB 
      NOCREATEROLE 
      NOREPLICATION; 
      
