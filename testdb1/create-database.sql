\echo '** creating database testdb01..'

-- Create database testdb1
CREATE DATABASE testdb1 WITH OWNER = postgres ENCODING = 'UTF8'  TABLESPACE = pg_default CONNECTION LIMIT = -1;

-- Revoke connection from all users
REVOKE CONNECT ON DATABASE testdb1 FROM public;
REVOKE CREATE ON SCHEMA public FROM PUBLIC;

\echo '** finish creating testdb01..'
