-- all grants for the user roles (just make user grants members of the appropriate database roles)
GRANT testdb1_app_connect TO testdb1_developer;
GRANT testdb1_app_read TO testdb1_developer;
GRANT testdb1_app_write TO testdb1_developer;
GRANT testdb1_app_read_reference TO testdb1_developer;
GRANT testdb1_app_connect TO testdb1_developer;
GRANT testdb1_app_read TO testdb1_developer;
GRANT testdb1_app_write TO testdb1_developer;
GRANT testdb1_app_read_reference TO testdb1_developer;
GRANT testdb1_developer TO testdb1_dba;
GRANT testdb1_advanced TO testdb1_dba;
GRANT ALL PRIVILEGES ON DATABASE testdb1 TO testdb1_dba;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO testdb1_dba;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO testdb1_dba;

-- developer grants (connect/read/read ref/write)
GRANT testdb1_app_connect TO testdb1_developer;
GRANT testdb1_app_read TO testdb1_developer;
GRANT testdb1_app_read_reference TO testdb1_developer;
GRANT testdb1_app_write TO testdb1_developer;

   -- developer should have app_connect, app_read, app_write, app_read_ref
GRANT testdb1_app_connect TO testdb1_developer;
GRANT testdb1_app_read TO testdb1_developer;
GRANT testdb1_app_write TO testdb1_developer;
GRANT testdb1_app_read_reference TO testdb1_developer;


-- advanced grants  (all developer grants + create table)
GRANT testdb1_app_connect TO testdb1_advanced;
GRANT testdb1_app_read TO testdb1_advanced;
GRANT testdb1_app_write TO testdb1_advanced;
GRANT testdb1_app_read_reference TO testdb1_advanced;
GRANT testdb1_app_create_objects TO testdb1_advanced;

-- dba grants  (everything a dba needs)
GRANT testdb1_app_connect TO testdb1_dba;
GRANT testdb1_app_read TO testdb1_dba;
GRANT testdb1_app_read_reference TO testdb1_dba;
GRANT testdb1_app_write TO testdb1_dba;
GRANT testdb1_app_create_objects TO testdb1_dba;
GRANT testdb1_developer TO testdb1_dba;
GRANT testdb1_advanced TO testdb1_dba;
GRANT ALL PRIVILEGES ON DATABASE testdb1 TO testdb1_dba;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO testdb1_dba;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO testdb1_dba;




