-- all grants for the database roles  (grants on actual database tables)
GRANT CONNECT ON DATABASE testdb1 TO testdb1_app_connect;
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_read;
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_write;
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_read_reference;
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_create_objects;
GRANT USAGE ON SCHEMA public TO testdb1_app_read;
GRANT SELECT ON ALL TABLES in SCHEMA public TO testdb1_app_read;
GRANT EXECUTE ON ALL functions in SCHEMA public TO testdb1_app_read;
GRANT USAGE ON ALL sequences in SCHEMA public TO testdb1_app_read;
GRANT USAGE ON SCHEMA public TO testdb1_app_write;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO testdb1_app_write;
GRANT USAGE ON SCHEMA public TO testdb1_app_read_reference;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO testdb1_app_read_reference;
GRANT REFERENCES ON ALL TABLES IN SCHEMA public TO testdb1_app_read_reference;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO testdb1_app_create_objects;
GRANT USAGE ON SCHEMA public TO testdb1_app_create_objects;
GRANT CREATE ON DATABASE testdb1 TO testdb1_app_create_objects;



-- app_connect grants
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_connect;


-- app_read grants
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_read;
GRANT USAGE ON SCHEMA public TO testdb1_app_read;
GRANT SELECT ON ALL TABLES in SCHEMA public TO testdb1_app_read;
GRANT EXECUTE ON ALL functions in SCHEMA public TO testdb1_app_read;
GRANT USAGE ON ALL sequences in SCHEMA public TO testdb1_app_read;


-- app_write grants
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_write;
GRANT USAGE ON SCHEMA public TO testdb1_app_write;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO testdb1_app_write;


-- app_read_reference grants
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_read_reference;
GRANT USAGE ON SCHEMA public TO testdb1_app_read_reference;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO testdb1_app_read_reference;
GRANT REFERENCES ON ALL TABLES IN SCHEMA public TO testdb1_app_read_reference;


-- app_create_objects grants
GRANT CONNECT ON  DATABASE testdb1 TO testdb1_app_create_objects;
GRANT USAGE ON SCHEMA public TO testdb1_app_create_objects;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO testdb1_app_create_objects;
GRANT CREATE ON DATABASE testdb1 TO testdb1_app_create_objects;
