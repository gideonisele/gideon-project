CREATE ROLE testdb1_dba WITH
	LOGIN
	NOSUPERUSER
	CREATEDB
	CREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'testdb1_dba';
	
	GRANT testdb1_advanced_role, testdb1_dba_role, testdb1_developer_role TO dba_user;


