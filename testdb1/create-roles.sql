\set dbname :testdb1

\i ../database-roles/app_read.sql
\i ../database-roles/app_read_reference.sql
\i ../database-roles/app_write.sql
\i ../database-roles/app_connect.sql
\i ../database-roles/app_create_objects.sql


-- user roles
\i ../user-roles/developer.sql
\i ../user-roles/advanced.sql
\i ../user-roles/dba.sql

